# 2019-03-19 Consistent Individualized Feature Attribution for Tree Ensembles (SHAP Values)
## Andrew Nicholls

ATOM is meeting on Tuesday, 19th March, 6:30pm, at Galvanize!

Our discussion this month will be lead by Andrew Nicholls. We’ll be discussing the paper “Consistent Individualized Feature Attribution for Tree Ensembles”.

https://arxiv.org/pdf/1802.03888.pdf

Understanding why a model makes a certain prediction can be as crucial as the model’s predictive performance. However, the highest predictive performance for large modern datasets is often achieved by complex models that even experts struggle to interpret, such as tree ensembles or deep learning models, creating a tension between performance and interpretability. SHAP values are one solution to this interpretability issue, showing improved computational performance and/or better consistency with human intuition than previous approaches regarding feature importance.

Some of the points we’ll discuss include:

My trained scikit-learn model already includes a feature importance function…shouldn’t I just use that?
I’ve heard of LIME, is that the next step?
What are SHapley Additive exPlanations (SHAP) values?
What does it mean that SHAP values are locally accurate and consistent?
Last month at ATOM I was convinced that more trees are better. Isn’t it going to be computationally intensive to calculate SHAP values for such a large number of trees?
Do SHAP values agree or disagree with human intuition?

About ATOM:
Advanced Topics on Machine learning ( ATOM ) is a learning and discussion group for cutting-edge machine learning techniques in the real world. We work through winning Kaggle competition entries or real-world ML projects, learning from those who have successfully applied sophisticated data science pipelines to complex problems.

As a discussion group, we strongly encourage participation, so be sure to read up about the topic of conversation beforehand !

ATOM can be found on PuPPy’s Slack under the channel #atom, and on PuPPy’s Meetup.com events.

We're kindly hosted by Galvanize (https://www.galvanize.com). Thank you !